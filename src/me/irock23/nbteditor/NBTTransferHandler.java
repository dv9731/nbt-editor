/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.nbteditor;

import java.awt.datatransfer.*;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.tree.*;

public class NBTTransferHandler extends TransferHandler {
	private static final long serialVersionUID = 8177637521766141434L;
	
	private final NBTEditor window;
	private static DataFlavor nodesFlavor = null;
	private static DataFlavor nixFileListFlavor = null;
	static {
		try {
			nodesFlavor = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType
					+ ";class=\"" + javax.swing.tree.DefaultMutableTreeNode[].class.getName() + "\"");
			nixFileListFlavor = new DataFlavor("text/uri-list;class=java.lang.String");
		} catch (ClassNotFoundException e) {}
	}
	
	public NBTTransferHandler(NBTEditor window) {
		this.window = window;
	}
	
	@Override
	public boolean canImport(TransferHandler.TransferSupport support) {
		if (!support.isDrop()) return false;
		//System.out.println(Arrays.toString(support.getDataFlavors()));
		if (support.isDataFlavorSupported(nodesFlavor)) {
			support.setShowDropLocation(true);
			support.setDropAction(MOVE);
			return ((JTree.DropLocation) support.getDropLocation()).getPath() != null;
		} else if (support.isDataFlavorSupported(DataFlavor.javaFileListFlavor) || support.isDataFlavorSupported(nixFileListFlavor)) {
			support.setShowDropLocation(false);
			support.setDropAction(COPY);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean importData(TransferHandler.TransferSupport support) {
		if (!canImport(support)) return false;
		if (support.isDataFlavorSupported(nodesFlavor)) {
			
		} else if (support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
			try {
				@SuppressWarnings("unchecked")
				List<File> files = (List<File>) support.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
				if (files.size() >= 1 && files.get(0).isFile())
					window.open(files.get(0));
				return true;
			} catch (Exception e) {
				return false;
			}
		} else if (support.isDataFlavorSupported(nixFileListFlavor)) {
			try {
				String data = (String) support.getTransferable().getTransferData(nixFileListFlavor);
				List<File> files = new ArrayList<File>();
				for (StringTokenizer st = new StringTokenizer(data, "\r\n"); st.hasMoreTokens();) {
					String token = st.nextToken();
					if (token.startsWith("#") || token.isEmpty()) continue;
					files.add(new File(new URI(token)));
				}
				if (files.size() >= 1 && files.get(0).isFile())
					window.open(files.get(0));
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}
	
	@Override
	protected Transferable createTransferable(JComponent c) {
		System.out.println("createTransferable");
		if (c instanceof JTree) {
			JTree tree = (JTree) c;
			TreePath[] paths = tree.getSelectionPaths();
			List<DefaultMutableTreeNode> nodes = new ArrayList<>();
			for (TreePath path : paths) nodes.add((DefaultMutableTreeNode) path.getLastPathComponent());
			List<DefaultMutableTreeNode> extra = new ArrayList<>();
			for (DefaultMutableTreeNode node : nodes) { //we don't need the nodes that are in another node to be moved
				for (DefaultMutableTreeNode n : nodes) {
					if (node != n && node.isNodeAncestor(n)) {
						extra.add(node);
						break;
					}
				}
			}
			nodes.removeAll(extra);
			
		}
		return null;
	}
	
	@Override
	public int getSourceActions(JComponent c) {
		return COPY_OR_MOVE;
	}
	
	public static class Transferable implements java.awt.datatransfer.Transferable {
		private final DefaultMutableTreeNode[] nodes;
		
		public Transferable(DefaultMutableTreeNode[] nodes) {
			this.nodes = nodes;
		}

		@Override
		public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
			if (flavor == null) throw new IllegalArgumentException("Flavor cannot be null");
			if (!isDataFlavorSupported(flavor)) throw new UnsupportedFlavorException(flavor);
			return nodes;
		}

		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return new DataFlavor[] {nodesFlavor};
		}

		@Override
		public boolean isDataFlavorSupported(DataFlavor flavor) {
			return Arrays.asList(getTransferDataFlavors()).contains(flavor);
		}
		
	}
	
	public static DefaultMutableTreeNode deepCloneNode(DefaultMutableTreeNode node) {
		DefaultMutableTreeNode clone = (DefaultMutableTreeNode) node.clone();
		clone.removeAllChildren();
		for (int i = 0; i < node.getChildCount(); i++)
			clone.add(deepCloneNode((DefaultMutableTreeNode) node.getChildAt(i)));
		return clone;
	}
}
