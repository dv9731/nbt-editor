/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.nbteditor;

import me.irock23.jnbt.*;

public class TagContainer {
	private final TagString name;
	private Tag tag;
	private int pos = -1;
	private Tag parent;
	
	public TagContainer(TagString name, Tag tag, Tag parent) {
		this.name = name;
		this.tag = tag;
		this.parent = parent;
		updatePos();
	}
	
	public void setName(String name) {
		if (isNBT()) {
			this.name.set(name);
		}
	}
	
	public void setName(TagString name) {
		setName(name.get());
	}
	
	public void updatePos() {
		if (tag != null && hasParent() && parent.isTagList()) {
			pos = parent.getAsTagList().indexOf(tag);
		}
	}
	
	public void setTag(Tag tag, int pos) {
		if (hasParent()) {
			if (parent.isTagList()) {
				TagList p = parent.getAsTagList();
				if (this.tag != null) {
					int i = p.indexOf(this.tag);
					p.remove(i);
					p.add(i, tag);
				} else if (pos > -1) p.add(pos, tag);
				else p.add(p.length().get() + pos + 1, tag);
				updatePos();
			} else {
				parent.getAsTagCompound().put(name, tag);
			}
		}
		this.tag = tag;
	}
	
	public void setTag(Tag tag) {
		setTag(tag, -1);
	}
	
	public void removeFromParent() {
		if (hasParent()) {
			if (parent.isTagList()) {
				updatePos();
				parent.getAsTagList().remove(pos);
			} else parent.getAsTagCompound().remove(name);
		}
	}
	
	public void addToParent() {
		if (hasParent()) {
			if (parent.isTagList()) {
				TagList p = parent.getAsTagList();
				if (pos > -1) p.add(pos, tag);
				else p.add(p.length().get() + pos + 1, tag);
			} else parent.getAsTagCompound().put(name, tag);
		}
	}
	
	public void setParent(Tag parent, int pos) {
		removeFromParent();
		this.parent = parent;
		this.pos = pos;
		addToParent();
	}
	
	public void setParent(Tag parent) {
		setParent(parent, -1);
	}
	
	public int getPos() {
		return pos;
	}
	
	public TagString getName() {
		return name;
	}
	
	public Tag getTag() {
		return tag;
	}
	
	public boolean hasParent() {
		return parent != null;
	}
	
	public Tag getParent() {
		return parent;
	}
	
	public boolean isRoot() {
		return !hasParent();
	}
	
	public boolean isNBT() {
		return (hasParent()) ? parent.isTagCompound() : name != null;
	}
	
	public NBT getNBT() {
		return (isNBT()) ? new NBT(name, tag) : null;
	}
	
	public String toString() {
		String s = (name != null ? name + " " : "") + "(" + tag.getTypeName() + ")";
		if (!tag.isTagList() && !tag.isTagCompound()) s += ": " + tag;
		return s;
	}
}
