/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.nbteditor;

import javax.swing.JComboBox;
import javax.swing.filechooser.FileFilter;

public class FileFilterComboBox extends JComboBox<String> {
	private static final long serialVersionUID = 5450369602308929094L;
	
	public FileFilterComboBox(FileFilter... filters) {
		super(getDescriptions(filters));
	}
	
	public void addItem(FileFilter filter) {
		addItem(filter.getDescription());
	}
	
	private static String[] getDescriptions(FileFilter[] filters) {
		String[] descriptions = new String[filters.length];
		for (int i = 0; i < filters.length; i++) descriptions[i] = filters[i].getDescription();
		return descriptions;
	}
}
