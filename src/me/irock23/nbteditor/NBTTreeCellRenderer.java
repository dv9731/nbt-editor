/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.nbteditor;

import java.awt.Component;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import me.irock23.jnbt.Tag;

public class NBTTreeCellRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 3595318750688250576L;
	
	private static Icon[] icons;
	
	public NBTTreeCellRenderer() {
		if (icons == null) {
			icons = new ImageIcon[Tag.TAG_STRING];
			for (int i = 0; i < icons.length; i++) {
				URL url = getClass().getResource("/res/tagicon" + (i+1) + ".png");
				if (url != null) icons[i] = new ImageIcon(url);
				else icons[i] = UIManager.getIcon("Tree.leafIcon");
			}
		}
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
		if (leaf) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			if (node.getUserObject() instanceof TagContainer) {
				Tag tag = ((TagContainer) node.getUserObject()).getTag();
				if (tag.getType() > 0 && tag.getType() <= Tag.TAG_STRING)
					setIcon(icons[tag.getType()-1]);
				else if (node.getAllowsChildren())
					setIcon(UIManager.getIcon("Tree.closedIcon"));
			}
		}
		return this;
	}
}
