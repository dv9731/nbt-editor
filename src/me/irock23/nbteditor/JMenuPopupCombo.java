/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.nbteditor;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class JMenuPopupCombo {
	JMenu menu = new JMenu();
	JPopupMenu popup = new JPopupMenu();
	List<Action> actions = new ArrayList<Action>();
	
	public JMenuPopupCombo(String text, Action... actions) {
		menu.setText(text);
		addActions(actions);
	}
	
	public void addActions(Action... actions) {
		for (Action a : actions) {
			this.actions.add(a);
			menu.add(new JMenuItem(a));
			popup.add(new JMenuItem(a));
		}
	}
	
	public void setItemEnabled(Action a, boolean enabled) {
		int i = actions.indexOf(a);
		menu.getItem(i).setEnabled(enabled);
		popup.getComponent(i).setEnabled(enabled);
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
	public JPopupMenu getPopupMenu() {
		return popup;
	}
}
