/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.nbteditor;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.*;

import me.irock23.jnbt.*;
import me.irock23.jnbt.mcpe.*;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class NBTEditor {
	public static final String VERSION = "0.6";
	public static final int NBT_STANDARD = 0;
	public static final int NBT_UNCOMPRESSED = 1;
	public static final int NBT_MCPE = 2;
	
	private FileFilter stdFF = new FileNameExtensionFilter("Standard NBT File (*.nbt|*.dat)", "nbt", "dat");
	private FileFilter uncFF = new FileNameExtensionFilter("Uncompressed NBT File (*.nbt|*.dat)", "nbt", "dat");
	private FileFilter mcpeFF = new FileNameExtensionFilter("MCPE NBT File (*.dat)", "dat");
	private JFileChooser fc;
	private File currentFile;
	private boolean newUneditedFile = true;//, isMCPE = false;
	private int fileType = NBT_STANDARD;
	private byte[] header; //MCPE file header
	private NBT mainNBT;
	private List<NBT> history = new ArrayList<>();
	/*private List<List<DefaultMutableTreeNode>> addedTags = new ArrayList<>();
	private List<List<DefaultMutableTreeNode>> removedTags = new ArrayList<>();
	private List<Map<DefaultMutableTreeNode, String[]>> renamedTags = new ArrayList<>();
	private List<Map<DefaultMutableTreeNode, Object[]>> modifiedTags = new ArrayList<>();*/
	private int historyIndex = -1, savedIndex = -1;

	private JFrame frame;
	private JMenuPopupCombo editMenus;
	private AbstractAction actionUndo, actionRedo;
	private JTree tree;
	private DefaultTreeModel treeModel;
	private DefaultMutableTreeNode treeRoot;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NBTEditor window = new NBTEditor();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NBTEditor() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {}
		
		fc = new JFileChooser();
		fc.setAcceptAllFileFilterUsed(false);
		fc.addChoosableFileFilter(stdFF);
		fc.addChoosableFileFilter(uncFF);
		fc.addChoosableFileFilter(mcpeFF);
		fc.setAcceptAllFileFilterUsed(true); //disabling and re-enabling puts it at bottom
		fc.setFileFilter(stdFF);
		
		frame = new JFrame();
		frame.setTitle("NBT Editor by Irock23");
		frame.setBounds(100, 100, 398, 336);
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (saveChanges()) System.exit(0);
			}
		});
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem(new AbstractAction("New") {
			private static final long serialVersionUID = -1232078971197352314L;
			public void actionPerformed(ActionEvent e) {
				startNew();
			}
		});
		mntmNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mnFile.add(mntmNew);
		
		JMenuItem mntmOpen = new JMenuItem(new AbstractAction("Open") {
			private static final long serialVersionUID = -1232078971197352314L;
			public void actionPerformed(ActionEvent e) {
				open();
			}
		});
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mnFile.add(mntmOpen);
		
		JMenuItem mntmSave = new JMenuItem(new AbstractAction("Save") {
			private static final long serialVersionUID = 4071900634955691532L;
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mnFile.add(mntmSave);
		
		JMenuItem mntmSaveAs = new JMenuItem(new AbstractAction("Save as...") {
			private static final long serialVersionUID = -6683645430639324817L;
			public void actionPerformed(ActionEvent e) {
				saveAs();
			}
		});
		mntmSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | KeyEvent.SHIFT_DOWN_MASK));
		mnFile.add(mntmSaveAs);
		
		JSeparator separator = new JSeparator();
		mnFile.add(separator);
		
		JMenuItem mntmExit = new JMenuItem(new AbstractAction("Exit") {
			private static final long serialVersionUID = -5537853653717827682L;
			public void actionPerformed(ActionEvent e) {
				if (saveChanges()) System.exit(0);
			}
		});
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mnFile.add(mntmExit);
		
		editMenus = new JMenuPopupCombo("Edit");
		menuBar.add(editMenus.getMenu());
		
		AbstractAction actionRename = new AbstractAction("Rename") {
			private static final long serialVersionUID = -5537853653717827682L;
			public void actionPerformed(ActionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
				if (node == null || !(node.getUserObject() instanceof TagContainer)) return;
				TagContainer tc = (TagContainer) node.getUserObject();
				if (!tc.isNBT()) return;
				String resp = (String) JOptionPane.showInputDialog(frame, "Please enter the new name for this tag",
						"Rename", JOptionPane.PLAIN_MESSAGE, null, null, tc.getName());
				if (resp != null) {
					tc.setName(resp);
					treeModel.nodeChanged(node);
					addToHistory();
				}
			}
		};
		actionRename.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		actionRename.setEnabled(false);
		AbstractAction actionDelete = new AbstractAction("Delete") {
			private static final long serialVersionUID = -5537853653717827682L;
			public void actionPerformed(ActionEvent e) {
				boolean removed = false;
				while (tree.getSelectionCount() > 0 && removeTag((DefaultMutableTreeNode) tree.getLastSelectedPathComponent()) != null)
					removed = true;
				if (removed) addToHistory();
			}
		};
		actionDelete.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		actionDelete.setEnabled(false);
		AbstractAction actionModify = new AbstractAction("Modify") {
			private static final long serialVersionUID = 6654371512662015634L;
			public void actionPerformed(ActionEvent e) {
				setTag((DefaultMutableTreeNode) tree.getLastSelectedPathComponent());
			}
		};
		actionModify.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
		actionModify.setEnabled(false);
		AbstractAction actionClear = new AbstractAction("Clear") {
			private static final long serialVersionUID = -9160140685029866312L;
			public void actionPerformed(ActionEvent e) {
				if (tree.getSelectionCount() > 0) {
					for (int i = 0; i < tree.getSelectionCount(); i++) {
						DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getSelectionPaths()[i].getLastPathComponent();
						((TagContainer) node.getUserObject()).getTag().clear();
						node.removeAllChildren();
						treeModel.nodeStructureChanged(node);
					}
					addToHistory();
				}
			}
		};
		actionClear.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0));
		actionClear.setEnabled(false);
		AbstractAction actionAddTag = new AbstractAction("Add Tag") {
			private static final long serialVersionUID = -9160140685029866312L;
			public void actionPerformed(ActionEvent e) {
				addTag((DefaultMutableTreeNode) tree.getLastSelectedPathComponent());
			}
		};
		actionAddTag.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		actionAddTag.setEnabled(false);
		AbstractAction actionInsertTag = new AbstractAction("Insert Tag Before") {
			private static final long serialVersionUID = -2844713881027385365L;
			public void actionPerformed(ActionEvent e) {
				insertNewTagBefore((DefaultMutableTreeNode) tree.getLastSelectedPathComponent());
			}
		};
		actionInsertTag.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, 0));
		actionInsertTag.setEnabled(false);
		actionUndo = new AbstractAction("Undo") {
			private static final long serialVersionUID = 2516098239236390233L;
			public void actionPerformed(ActionEvent e) {
				goBackInHistory();
			}
		};
		actionUndo.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		actionUndo.setEnabled(false);
		actionRedo = new AbstractAction("Redo") {
			private static final long serialVersionUID = 5375316431977862781L;
			public void actionPerformed(ActionEvent e) {
				goForwardInHistory();
			}
		};
		actionRedo.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		actionRedo.setEnabled(false);
		
		editMenus.addActions(actionRename, actionDelete, actionModify, actionClear, actionAddTag, actionInsertTag, actionUndo, actionRedo);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem(new AbstractAction("About") {
			private static final long serialVersionUID = 935843370412248907L;
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame,
						"NBT Editor\nAuthor: David Vick\nVersion: " + VERSION + "\nNBT Version: " + NBT.VERSION,
						"About", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		mnHelp.add(mntmAbout);
		
		tree = new JTree(treeRoot = new DefaultMutableTreeNode());
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
				editMenus.setItemEnabled(actionRename, tree.getSelectionCount() == 1 && node != null && ((TagContainer) node.getUserObject()).isNBT());
				editMenus.setItemEnabled(actionDelete, node != null && node != treeRoot);
				editMenus.setItemEnabled(actionModify, tree.getSelectionCount() == 1 && node != null && node != treeRoot);
				editMenus.setItemEnabled(actionClear, node != null/* && node.getAllowsChildren()*/);
				editMenus.setItemEnabled(actionAddTag, tree.getSelectionCount() == 1 && node != null);
				editMenus.setItemEnabled(actionInsertTag, tree.getSelectionCount() == 1 && node != null);
			}
		});
		tree.setComponentPopupMenu(editMenus.getPopupMenu());
		tree.setShowsRootHandles(true);
		tree.setCellRenderer(new NBTTreeCellRenderer());
		tree.setDragEnabled(true);
		tree.setDropMode(DropMode.ON_OR_INSERT);
		tree.setTransferHandler(new NBTTransferHandler(this));
		/*tree.setDropTarget(new DropTarget() {
			private static final long serialVersionUID = -8361630732566542322L;
			@SuppressWarnings("unchecked")
			public synchronized void drop(DropTargetDropEvent e) {
				e.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
				List<File> files = null;
				if (e.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
					try {
						files = (List<File>) e.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
					} catch (Exception e1) {}
				} else {
					try {
						String data = (String) e.getTransferable().getTransferData(new DataFlavor("text/uri-list;class=java.lang.String"));
						files = new ArrayList<>();
						for (StringTokenizer st = new StringTokenizer(data, "\r\n"); st.hasMoreTokens();) {
							String token = st.nextToken();
							if (token.startsWith("#") || token.isEmpty()) continue;
							files.add(new File(new URI(token)));
						}
					} catch (Exception e1) {}
				}
				if (files != null) {
					System.out.println(Arrays.toString(files.toArray()));
				} else {
					System.out.println(Arrays.toString(e.getCurrentDataFlavors()));
					try {
						System.out.println(e.getTransferable().getTransferData(new DataFlavor(DataFlavor.javaSerializedObjectMimeType
								+ ";class=java.lang.String"
						/*+ ";class=\"" + javax.swing.tree.DefaultMutableTreeNode[].class.getName() + "\""*\/)));
					} catch (Exception e1) {
						System.out.println(false);
					}
				}
			}
		});*/
		treeModel = (DefaultTreeModel) tree.getModel();
		frame.getContentPane().add(new JScrollPane(tree));
		
		startNew();
	}
	
	private void insertNewTagBefore(DefaultMutableTreeNode sibling) {
		if (sibling == null) return;
		DefaultMutableTreeNode parent = (DefaultMutableTreeNode) sibling.getParent();
		if (parent != null) addTag(parent, parent.getIndex(sibling));
	}
	
	private void addTag(DefaultMutableTreeNode parent) {
		addTag(parent, -1);
	}
	
	private void addTag(DefaultMutableTreeNode parent, int pos) {
		if (parent == null || !(parent.getUserObject() instanceof TagContainer)) return;
		TagContainer tc = (TagContainer) parent.getUserObject();
		TagContainer ntc = null;
		boolean parentHasTag = false;
		if (tc.getTag().isTagCompound()) {
			String resp = JOptionPane.showInputDialog(frame,
					"Enter the name for this tag:", "Add Tag", JOptionPane.PLAIN_MESSAGE);
			if (resp == null) return;
			TagString name = new TagString(resp);
			parentHasTag = tc.getTag().getAsTagCompound().get().containsKey(name);
			ntc = new TagContainer(name, null, tc.getTag());
		} else if (tc.getTag().isTagList()) {
			ntc = new TagContainer(null, null, tc.getTag());
		} else {
			if (!parent.isRoot()) {
				DefaultMutableTreeNode pp = (DefaultMutableTreeNode) parent.getParent();
				addTag(pp, pp.getIndex(parent));
			}
			//else //should never get here unless the NBT had a tag other than TAG_Compound, which is technically invalid
			return;
		}
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(ntc);
		if (setTag(node, false, pos)) {
			if (parentHasTag) {
				for (int i = 0; i < parent.getChildCount(); i++) {
					DefaultMutableTreeNode n = (DefaultMutableTreeNode) parent.getChildAt(i);
					TagContainer stc = (TagContainer) node.getUserObject();
					if (stc.getName().equals(ntc.getName())) {
						treeModel.removeNodeFromParent(n);
						break;
					}
				}
			}
			treeModel.insertNodeInto(node, parent, (pos < 0) ? parent.getChildCount() + pos + 1 : pos);
			tree.expandPath(new TreePath(((DefaultMutableTreeNode) node.getParent()).getPath()));
		}
	}
	
	private boolean setTag(DefaultMutableTreeNode node) {
		return setTag(node, true, -1);
	}
	
	private boolean setTag(DefaultMutableTreeNode node, boolean modify, int pos) {
		if (node == null || !(node.getUserObject() instanceof TagContainer)) return false;
		if (node == treeRoot) {
			JOptionPane.showMessageDialog(frame, "The root tag cannot be modified", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		TagContainer tc = (TagContainer) node.getUserObject();
		int type;
		JPanel typesMsg = new JPanel(new GridLayout(0, 1));
		JLabel typesLabel = new JLabel("Choose the type for this tag:");
		typesMsg.add(typesLabel);
		JComboBox<String> typesBox = new JComboBox<>(Arrays.copyOfRange(Tag.NAMES, 1, Tag.DEFINED_TAGS));
		typesBox.addAncestorListener(new FocusRequester());
		typesMsg.add(typesBox);
		if (!tc.getParent().isTagList()) {
			if (tc.getTag() != null) typesBox.setSelectedIndex(tc.getTag().getType() - 1);
			int opt = JOptionPane.showConfirmDialog(frame, typesMsg, (modify ? "Modify" : "Add") + " Tag",
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
			if (opt != JOptionPane.OK_OPTION) return false;
			else type = typesBox.getSelectedIndex() + 1;
		} else type = tc.getParent().getAsTagList().getListType();
		boolean success = true, structured = true;
		if (type == Tag.TAG_LIST) {
			node.setAllowsChildren(true);
			byte oldType = (tc.getTag() != null && tc.getTag().isTagList()) ? tc.getTag().getAsTagList().getListType() : -1;
			typesLabel.setText("Choose the type for this list:");
			typesBox.setSelectedIndex(oldType > 0 ? oldType - 1 : 0);
			int listType, opt = JOptionPane.showConfirmDialog(frame, typesMsg, (modify ? "Modify" : "Add") + " Tag",
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
			if (opt != JOptionPane.OK_OPTION) return false;
			else listType = typesBox.getSelectedIndex() + 1;
			if (listType != oldType) {
				node.removeAllChildren();
				tc.setTag(new TagList((byte) listType), pos);
			}
		} else if (type == Tag.TAG_COMPOUND) {
			node.setAllowsChildren(true);
			if (tc.getTag() == null || !tc.getTag().isTagCompound()) {
				node.removeAllChildren();
				tc.setTag(new TagCompound(), pos);
			}
		} else {
			if (!node.getAllowsChildren()) structured = false;
			else node.setAllowsChildren(false);
			String value = (!modify || tc.getTag().getType() == Tag.TAG_LIST || tc.getTag().getType() == Tag.TAG_COMPOUND) ? "" : tc.getTag().toString();
			String resp = (String) JOptionPane.showInputDialog(frame,
					"Enter the" + (modify ? " new" : "") + " value for this " + Tag.NAMES[type] + ":",
					(modify ? "Modify" : "Add") + " Tag",
					JOptionPane.PLAIN_MESSAGE, null, null, value);
			if (resp == null) return false;
			try{
				switch (type) {
					case Tag.TAG_BYTE:
						tc.setTag(new TagByte(Byte.parseByte(resp.trim())), pos); break;
					case Tag.TAG_SHORT:
						tc.setTag(new TagShort(Short.parseShort(resp.trim())), pos); break;
					case Tag.TAG_INT:
						tc.setTag(new TagInt(Integer.parseInt(resp.trim())), pos); break;
					case Tag.TAG_LONG:
						tc.setTag(new TagLong(Long.parseLong(resp.trim())), pos); break;
					case Tag.TAG_FLOAT:
						tc.setTag(new TagFloat(Float.parseFloat(resp.trim())), pos); break;
					case Tag.TAG_DOUBLE:
						tc.setTag(new TagDouble(Double.parseDouble(resp.trim())), pos); break;
					case Tag.TAG_STRING:
						tc.setTag(new TagString(resp), pos); break;
					case Tag.TAG_BYTE_ARRAY:
						String[] parts = resp.replace("[", "").replace("]", "").trim().split("\\s*,\\s*");
						byte[] bytes = new byte[parts.length];
						for (int i = 0; i < bytes.length; i++)
							bytes[i] = Byte.parseByte(parts[i]);
						tc.setTag(new TagByteArray(bytes), pos);
						break;
					case Tag.TAG_INT_ARRAY:
						parts = resp.replace("[", "").replace("]", "").trim().split("\\s*,\\s*");
						int[] ints = new int[parts.length];
						for (int i = 0; i < ints.length; i++)
							ints[i] = Integer.parseInt(parts[i]);
						tc.setTag(new TagIntArray(ints), pos);
				}
			} catch (NumberFormatException ex) {
				success = false;
				JOptionPane.showMessageDialog(frame, "That was an invalid value for the specified type.",
						"Invalid Value", JOptionPane.ERROR_MESSAGE);
			}
		}
		if (success) {
			if (modify) {
				if (structured) treeModel.nodeStructureChanged(node);
				else treeModel.nodeChanged(node);
			}
			addToHistory();
		}
		return success;
	}
	
	private TagContainer removeTag(DefaultMutableTreeNode node) {
		if (node == null || !(node.getUserObject() instanceof TagContainer)) return null;
		TagContainer tc = (TagContainer) node.getUserObject();
		if (tc.hasParent()) {
			Tag parent = tc.getParent();
			if (parent.isTagList())
				parent.getAsTagList().remove(tc.getTag());
			else
				parent.getAsTagCompound().remove(tc.getName());
			treeModel.removeNodeFromParent(node);
			//addToHistory();
		} else {
			JOptionPane.showMessageDialog(frame, "The root tag cannot be removed!", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		return tc;
	}
	
	private void startNew() {
		if (saveChanges()) {
			currentFile = null;
			mainNBT = new NBT("", new TagCompound());
			clearHistory();
			addToHistory();
			savedIndex = -1;
			newUneditedFile = true;
			updateTitle();
			generateTree();
		}
	}
	
	private void open() {
		if (!saveChanges()) return; 
		fc.setDialogTitle("Open NBT File");
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			currentFile = fc.getSelectedFile();
			//isMCPE = fc.getFileFilter() == mcpeFF;
			fileType = (fc.getFileFilter() == uncFF) ? NBT_UNCOMPRESSED : (fc.getFileFilter() == mcpeFF) ? NBT_MCPE : NBT_STANDARD;
			openFile(-1);
		}
	}
	
	void open(File file) {
		if (file == null || !saveChanges()) return;
		currentFile = file;
		JPanel msg = new JPanel(new GridLayout(0, 1)); //could be (2, 1), but 0 allows as many as you like, so why not?
		msg.add(new JLabel("Choose the type of file:"));
		FileFilterComboBox combo = new FileFilterComboBox(stdFF, uncFF, mcpeFF);
		combo.addAncestorListener(new FocusRequester());
		msg.add(combo);
		int opt = JOptionPane.showConfirmDialog(frame, msg, "Open File", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (opt == JOptionPane.OK_OPTION) {
			fileType = combo.getSelectedIndex();
			openFile(-1);
		}
	}
	
	private void openFile(int headerSize) {
		NBTInputStream in = null;
		boolean headerWorked = false;
		try {
			if (fileType == NBT_MCPE) {
				if (headerSize < 0) {
					if (currentFile.getName().equalsIgnoreCase("entities.dat")) headerSize = 12;
					else headerSize = 8;
				}
				in = new NBTPEInputStream(new FileInputStream(currentFile), headerSize);
				header = ((NBTPEInputStream) in).getHeader();
				headerWorked = true;
				Tag.setDefaultByteOrder(ByteOrder.LITTLE_ENDIAN);
			} else {
				in = new NBTInputStream(new FileInputStream(currentFile), fileType == NBT_STANDARD);
				Tag.setDefaultByteOrder(ByteOrder.BIG_ENDIAN);
			}
			mainNBT = in.readNBT();
			clearHistory();
			addToHistory();
			savedIndex = 0;
			updateTitle();
			generateTree();
			tree.expandPath(new TreePath(treeRoot.getPath()));
		} catch (IOException e1) {
			if (fileType == NBT_MCPE && !headerWorked) {
				String resp = JOptionPane.showInputDialog(frame,
						"MCPE NBT files contain a header before the actual NBT data.\nThe header size was unable to be determined"
						+ " automatically.\nPlease input the header size for this file.",
						"MCPE Header", JOptionPane.QUESTION_MESSAGE);
				if (resp != null) try {
					openFile(Integer.parseInt(resp));
				} catch (NumberFormatException e2) {
					JOptionPane.showMessageDialog(frame, "The input data was not a valid integer.",
							"Invalid Integer", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(frame,
						"An error occured opening " + currentFile.getName() + ". This might not be a valid NBT file.",
						"Error Opening File", JOptionPane.ERROR_MESSAGE);
			}
		} finally {
			try {if (in != null) in.close();} catch (Exception e1) {}
		}
	}
	
	private boolean save() {
		if (currentFile != null) {
			NBTOutputStream out = null;
			try {
				if (fileType == NBT_MCPE) {
					if (header == null) {
						String resp = JOptionPane.showInputDialog(frame,
								"MCPE NBT files contain a header before the actual NBT data."
								+ "\nPlease input the header data (length data will be added):",
								"MCPE Header", JOptionPane.QUESTION_MESSAGE);
						if (resp != null) {
							String[] parts = resp.trim().split("\\s*,\\s*");
							header = new byte[parts.length + 4];
							if (resp.trim().length() > 0) {
								try {
									for (int i = 0; i < parts.length; i++)
										header[i] = Byte.parseByte(parts[i]);
								} catch (NumberFormatException e) {
									header = null;
									JOptionPane.showMessageDialog(frame, "The input data was not valid.",
											"Invalid Header Size", JOptionPane.ERROR_MESSAGE);
									return false;
								}
							}
						} else return false;
					}
					if (header.length >= 4) {
						ByteBuffer b = (ByteBuffer) ByteBuffer.wrap(header).order(ByteOrder.LITTLE_ENDIAN).position(header.length - 4);
						b.putInt(mainNBT.size(ByteOrder.LITTLE_ENDIAN));
					}
					out = new NBTPEOutputStream(new FileOutputStream(currentFile), header);
				} else out = new NBTOutputStream(new FileOutputStream(currentFile), fileType == NBT_STANDARD);
				out.writeNBT(mainNBT);
				savedIndex = historyIndex;
				updateTitle();
				return true;
			} catch (IOException e) {
				JOptionPane.showMessageDialog(frame,
						"An error occured trying to write to " + currentFile.getAbsolutePath() + ".",
						"Error Saving File", JOptionPane.ERROR_MESSAGE);
				return false;
			} finally {
				try {if (out != null) out.close();} catch (Exception e) {}
			}
		} else return saveAs();
	}
	
	private boolean saveAs() {
		fc.setDialogTitle("Save NBT File As...");
		if (fc.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
			fileType = (fc.getFileFilter() == uncFF) ? NBT_UNCOMPRESSED : (fc.getFileFilter() == mcpeFF) ? NBT_MCPE : NBT_STANDARD;
			currentFile = fc.getSelectedFile();
			String path = currentFile.getAbsolutePath();
			if (path.lastIndexOf('.') <= path.replace('\\', '/').lastIndexOf('/'))
				currentFile = new File(path.concat(fileType == NBT_MCPE ? ".dat" : ".nbt"));
			//isMCPE = fc.getFileFilter() == mcpeFF;
			updateTitle();
			return save();
		}
		return false;
	}
	
	private boolean saveChanges() {
		if (newUneditedFile || savedIndex == historyIndex) return true;
		int opt = JOptionPane.showOptionDialog(frame, "There are unsaved changes. Would you like to save?",
				"Unsaved Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				null, null, null);
		return (opt == JOptionPane.YES_OPTION) ? save() : opt == JOptionPane.NO_OPTION;
	}
	
	//this looked so much better before it reused nodes :(
	void generateTree(TagString name, Tag tag, DefaultMutableTreeNode node, Tag parent, boolean reuse) {
		node.setUserObject(new TagContainer(name, tag, parent));
		if (tag.isTagList()) {
			node.setAllowsChildren(true);
			List<Tag> tags = tag.getAsTagList().get();
			int keep = (reuse) ? tags.size() : 0;
			for (int i = node.getChildCount() - 1; i >= keep; i--)
				treeModel.removeNodeFromParent((DefaultMutableTreeNode) node.getChildAt(i)); //remove nodes we won't reuse
			for (int i = 0; i < tags.size(); i++) {
				DefaultMutableTreeNode n = null;
				if (i < node.getChildCount()) n = (DefaultMutableTreeNode) node.getChildAt(i); //reuse nodes (so not everything collapses)
				else {
					n = new DefaultMutableTreeNode();
					treeModel.insertNodeInto(n, node, node.getChildCount()); //node.add(n);
				}
				generateTree(null, tags.get(i), n, tag, reuse);
			}
		} else if (tag.isTagCompound()) {
			node.setAllowsChildren(true);
			Collection<NBT> tags = tag.getAsTagCompound().get().values();
			int keep = (reuse) ? tags.size() : 0;
			for (int i = node.getChildCount() - 1; i >= keep; i--)
				treeModel.removeNodeFromParent((DefaultMutableTreeNode) node.getChildAt(i)); //remove nodes we won't reuse
			NBT[] tagz = tags.toArray(new NBT[tags.size()]);
			for (int i = 0; i < tagz.length; i++) {
				NBT t = tagz[i];
				DefaultMutableTreeNode n = null;
				if (i < node.getChildCount()) n = (DefaultMutableTreeNode) node.getChildAt(i);
				else {
					n = new DefaultMutableTreeNode();
					treeModel.insertNodeInto(n, node, node.getChildCount());
				}
				generateTree(t.getName(), t.getTag(), n, tag, reuse);
			}
		} else node.setAllowsChildren(false);
		treeModel.nodeChanged(node);
	}
	
	private void generateTree(boolean reuse) {
		//treeRoot.removeAllChildren();
		generateTree(mainNBT.getName(), mainNBT.getTag(), treeRoot, null, reuse);
		//treeModel.reload();
	}
	
	private void generateTree() {
		generateTree(false);
	}
	
	/*public void clearHistory() {
		addedTags.clear();
		removedTags.clear();
		renamedTags.clear();
		modifiedTags.clear();
		historyIndex = -1;
		savedIndex = -1;
	}
	
	public void newHistory() {
		historyIndex++;
		while (addedTags.size() > historyIndex) {
			addedTags.remove(addedTags.size() - 1);
			removedTags.remove(addedTags.size() - 1);
			renamedTags.remove(addedTags.size() - 1);
			modifiedTags.remove(addedTags.size() - 1);
		}
		addedTags.add(new ArrayList<DefaultMutableTreeNode>());
		removedTags.add(new ArrayList<DefaultMutableTreeNode>());
		renamedTags.add(new HashMap<DefaultMutableTreeNode, String[]>());
		modifiedTags.add(new HashMap<DefaultMutableTreeNode, Object[]>());
	}
	
	public void goBackInHistory() {
		if (historyIndex > 0) {
			for (DefaultMutableTreeNode node : addedTags.get(historyIndex)) {
				TagContainer tc = (TagContainer) node.getUserObject();
				tc.removeFromParent();
				treeModel.removeNodeFromParent(node);
			}
			for (DefaultMutableTreeNode node : removedTags.get(historyIndex)) {
				TagContainer tc = (TagContainer) node.getUserObject();
				tc.addToParent();
				DefaultMutableTreeNode parent = tc.getParentNode();
				treeModel.insertNodeInto(node, parent, (tc.getPos() < 0) ? parent.getChildCount() : tc.getPos());
			}
			for (Entry<DefaultMutableTreeNode, String[]> e : renamedTags.get(historyIndex).entrySet()) {
				DefaultMutableTreeNode node = e.getKey();
				String oldName = e.getValue()[0];
				((TagContainer) node.getUserObject()).setName(oldName);
				treeModel.nodeChanged(node);
			}
			for (Entry<DefaultMutableTreeNode, Object[]> e : modifiedTags.get(historyIndex).entrySet()) {
				DefaultMutableTreeNode node = e.getKey();
				int oldType = (Integer) e.getValue()[0];
				Object oldValue = e.getValue()[1];
				TagContainer tc = (TagContainer) node.getUserObject();
				switch (oldType) {
					case Tag.TAG_BYTE: tc.setTag(new TagByte((Byte) oldValue)); break;
					case Tag.TAG_SHORT: tc.setTag(new TagShort((Short) oldValue)); break;
					case Tag.TAG_INT: tc.setTag(new TagInt());
				}
			}
			historyIndex--;
		}
	}
	
	public void goForwardInHistory() {
		
	}*/
	
	public void clearHistory() {
		history.clear();
		historyIndex = -1;
		savedIndex = -1;
	}
	
	public void addToHistory() {
		historyIndex++;
		while (history.size() > historyIndex) history.remove(history.size() - 1);
		history.add(mainNBT.clone());
		newUneditedFile = false;
		updateHistoryStuff();
	}
	
	public void goBackInHistory() {
		if (historyIndex > 0) {
			mainNBT = history.get(--historyIndex).clone();
			updateHistoryStuff();
			generateTree(true);
		}
	}
	
	public void goForwardInHistory() {
		if (historyIndex < history.size() - 1) {
			mainNBT = history.get(++historyIndex).clone();
			updateHistoryStuff();
			generateTree(true);
		}
	}
	
	private void updateHistoryStuff() {
		editMenus.setItemEnabled(actionUndo, historyIndex > 0);
		editMenus.setItemEnabled(actionRedo, historyIndex < history.size() - 1);
		updateTitle();
	}
	
	private void updateTitle() {
		String ind = (!newUneditedFile && savedIndex != historyIndex) ? "*" : "";
		if (currentFile != null)
			frame.setTitle(ind + currentFile.getName() + " - NBT Editor");
		else if (newUneditedFile)
			frame.setTitle("NBT Editor");
		else
			frame.setTitle(ind + "Untitled - NBT Editor");
	}
}
